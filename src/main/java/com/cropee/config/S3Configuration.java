package com.cropee.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3AsyncClient;

import java.net.URI;

@Configuration
public class S3Configuration {
    @Value("${s3.url}")
    private String url;
    @Value("${s3.region}")
    private String region;
    @Value("${s3.access-key}")
    private String accessKey;
    @Value("${s3.secret}")
    private String secret;

    @Bean
    S3AsyncClient s3Client() {
        return S3AsyncClient.builder()
                .endpointOverride(URI.create(url))
                .region(Region.of(region))
                .credentialsProvider(StaticCredentialsProvider.create(
                        AwsBasicCredentials.create(
                                accessKey,
                                secret
                        )
                ))
                .build();
    }
}
