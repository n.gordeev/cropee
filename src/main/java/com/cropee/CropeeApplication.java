package com.cropee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CropeeApplication {

    public static void main(String[] args) {
        SpringApplication.run(CropeeApplication.class, args);
    }

}
