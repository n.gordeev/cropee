package com.cropee.util;

import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;
import org.bytedeco.javacpp.Loader;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

@Slf4j
public class OCVResize {
    static {
        log.info("Loading opencv");
        final long t0 = System.currentTimeMillis();
        Loader.load(org.bytedeco.opencv.opencv_java.class);
        log.info("opencv loaded in {}ms", (System.currentTimeMillis() - t0));
    }

    public static byte[] resize(byte[] source, ImageExtension targetExtension, int width, int height) {
        @Cleanup("release") final MatOfByte sourceMat = new MatOfByte(source);
        @Cleanup("release") final MatOfByte outputMat = new MatOfByte();
        @Cleanup("release") final Mat decodedSourceMat = Imgcodecs.imdecode(sourceMat, Imgcodecs.IMREAD_UNCHANGED);
        @Cleanup("release") final Mat resizedDecodedMat = new Mat();
        Imgproc.resize(decodedSourceMat, resizedDecodedMat, new Size(width, height));

        final boolean success = Imgcodecs.imencode(getOpenCVExt(targetExtension), resizedDecodedMat, outputMat);
        if (!success) {
            throw new IllegalStateException("Failed to encode image");
        }

        return outputMat.toArray();
    }

    private static String getOpenCVExt(ImageExtension ext) {
        return "." + ext.name().toLowerCase();
    }

    public enum ImageExtension {
        JPG, PNG
    }
}
