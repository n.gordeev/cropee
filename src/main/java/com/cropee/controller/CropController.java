package com.cropee.controller;

import com.cropee.service.ProcessingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.ByteBuffer;

@Slf4j
@RestController
@RequiredArgsConstructor
public class CropController {
    private final ProcessingService service;

    @GetMapping("/{width:\\d+}x{height:\\d+}/{bucket}/{key}")
    public Mono<ResponseEntity<Flux<ByteBuffer>>> get(@PathVariable int width,
                                    @PathVariable int height,
                                    @PathVariable String bucket,
                                    @PathVariable String key) {
        log.info("resizing {} / {} to size {}x{}", bucket, key, width, height);
        return Mono.just(
                ResponseEntity.ok()
                        .header("Content-Type", "image/png")
                        .body(service.get(bucket, key, width, height))
        );
    }
}
