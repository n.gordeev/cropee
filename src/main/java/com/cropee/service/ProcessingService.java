package com.cropee.service;

import com.cropee.util.OCVResize;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import software.amazon.awssdk.core.async.AsyncRequestBody;
import software.amazon.awssdk.core.async.AsyncResponseTransformer;
import software.amazon.awssdk.http.SdkHttpResponse;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.NoSuchKeyException;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

import java.nio.ByteBuffer;
import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProcessingService {
    private static final String RESIZED_BUCKET = "resized";
    private final S3AsyncClient s3;

    public Flux<ByteBuffer> get(String bucket, String key, int width, int height) {
        final String resizedKey = "/%d_%d/%s".formatted(width, height, key.replaceAll("^/", ""));
        return getFileFlux(RESIZED_BUCKET, resizedKey)
                .onErrorResume(
                        NoSuchKeyException.class,
                        e -> getFile(bucket, key)
                                .map(file -> OCVResize.resize(
                                        file.array(),
                                        OCVResize.ImageExtension.PNG,
                                        width,
                                        height
                                ))
                                .flatMap(bytes -> uploadResized(bytes, RESIZED_BUCKET, resizedKey))
                                .map(ByteBuffer::wrap)
                                .flux()
                );
    }

    private Mono<byte[]> uploadResized(byte[] bytes, String bucket, String key) {
        return Mono.fromFuture(s3.putObject(
                PutObjectRequest.builder()
                        .bucket(bucket)
                        .key(key)
                        .build(),
                AsyncRequestBody.fromBytes(bytes)
        )).thenReturn(bytes);
    }

    private Flux<ByteBuffer> getFileFlux(String bucket, String key) {
        return Mono.fromFuture(s3.getObject(
                        GetObjectRequest.builder()
                                .bucket(bucket)
                                .key(key)
                                .build(),
                        AsyncResponseTransformer.toPublisher()
                ))
                .flatMapMany(response -> {
                    final SdkHttpResponse sdkResponse = response.response().sdkHttpResponse();
                    if (sdkResponse == null || !sdkResponse.isSuccessful()) {
                        return Flux.error(new IllegalStateException("Failed to download file: " + response.response()));
                    }

                    return Flux.from(response);
                });
    }

    private Mono<ByteBuffer> getFile(String bucket, String key) {
        return Mono.fromFuture(s3.getObject(
                        GetObjectRequest.builder()
                                .bucket(bucket)
                                .key(key)
                                .build(),
                        AsyncResponseTransformer.toPublisher()
                ))
                .flatMap(response -> {
                    final SdkHttpResponse sdkResponse = response.response().sdkHttpResponse();
                    if (sdkResponse == null || !sdkResponse.isSuccessful()) {
                        return Mono.error(new IllegalStateException("Failed to download file: " + response.response()));
                    }

                    // FIXME 14.10.2022:
                    final Long contentLength = Objects.requireNonNull(response.response().contentLength());
                    final ByteBuffer buffer = ByteBuffer.allocate(Math.toIntExact(contentLength));
                    return Flux.from(response)
                            .reduce(buffer, ByteBuffer::put);
                });
    }
}
